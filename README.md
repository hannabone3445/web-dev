# Introduction to JavaScript

Every browser ships a JavaScript Interpreter also called a JavaScript Engine. V8 is the JavaScript engine designed by Google and used in the Google Chrome browser while SpiderMonkey is a JavaScript engine developed by Mozilla for their Firefox browser.

Since JavaScript engine designed by every browser is different, ECMA standardizes features of JavaScript. This standard is known as ECMAScript (pronounced as ek-ma-script). Whenever ECMA adds a feature to this JavaScript standard, the browser has to add it in their JavaScript engine to stay in the competition (though this process is very slow).

[JavaScript is a very easy language](https://itmaster-soft.com/en/vue-js-development-services) to learn and fun to write. Every year, new features are added to ECMAScript which brings JavaScript one more step closer to dominate the planet. The latest major revision of JavaScript is ES6 or ECMAScript 6 or ECMAScript 2015 which has dumped a ton of features to make it more fun to code in. At the moment, JavaScript supports the OOP paradigm very well and can be used in functional programming as well.

Mozilla is an open-source foundation that documents JavaScript very well on their developer documentation AKA Mozilla Developer Network or MDN. It is one of the top online destinations to learn JavaScript, though there are other online resources as well. If you want to take a look at the JavaScript specifications and learn simple tutorials, visit MDN Documentation.

Read more - [https://itmaster-soft.com/en/laravel-vs-django-vs-node-making-the-best-backend-choice](https://itmaster-soft.com/en/laravel-vs-django-vs-node-making-the-best-backend-choice)